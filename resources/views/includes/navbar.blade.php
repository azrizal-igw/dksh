    <!-- navbar -->
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
        <a class="navbar-brand" href="{{ route('get-index') }}">DKSH - Gryphon Telematics</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    
                </li>
            </ul>
            <form id="searchForm" class="form-inline my-2 my-lg-0" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input class="form-control mr-sm-2" type="text" name="keyword" placeholder="Search" value="{{ $keyword }}" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
            </form>
        </div>
    </nav>