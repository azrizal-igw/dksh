<!DOCTYPE html>
<html>

@include('includes/head')

<body>
    @include('includes/navbar')

	<div id="map"></div>  



	@if (!empty($get_a) || !empty($get_b)) 


		<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
	    <script>
		function initMap() {
			var cyberjaya = {lat: 2.9213, lng: 101.6559};
			
			/* base map */
			var map = new google.maps.Map(document.getElementById('map'), {
				zoom: 12,
				center: cyberjaya
			});
			
			var bounds = new google.maps.LatLngBounds();
			
			var props = '<?php echo json_encode(array_merge($get_a, $get_b), JSON_UNESCAPED_SLASHES); ?>';
			
			var markers = JSON.parse(props).map(function(props, i) {
				bounds.extend(props.coords);
				
				return new google.maps.Marker({
					position: props.coords,
					map: map,
					icon: 'food-truck-icon.png',
					//content: props.content
					title: props.content
					//label: props.content
				});
			});
			
			$.each( markers, function( index, value ){
				console.log(value)
				
				var infoWindow = new google.maps.InfoWindow({
					content: value.title
				});
				infoWindow.open(map, value);
				
				value.addListener('mouseover', function() {
					infoWindow.open(map, value);
				});
			});
			
			// Add a marker clusterer to manage the markers.
			var markerCluster = new MarkerClusterer(map, markers, {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
			
			map.fitBounds(bounds);
						
			function addMarker(props) {
				var marker = new google.maps.Marker({
					position: props.coords,
					map: map,
					icon: 'food-truck-icon-2.png',
					title: props.content
				});
				
			}

		}
		
	    </script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
		<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
	    <script async defer
	    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDOMFyx6Lje5OIy7gCJbCUGKDLrRfLnCAU&callback=initMap">
		</script>

	@else
		@include('includes/footer')


	@endif

</body>



</html>

