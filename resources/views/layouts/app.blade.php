<!DOCTYPE html>
<html>

@include('includes/head')

<body>
    @include('includes/navbar')
    @yield('content')
</body>

@include('includes/footer')

</html>





