<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

class HomeController extends Controller
{

    public function getIndex()
    {
    	$keyword = null;
        return view('home', compact('keyword'));
    }

    public function postIndex(Request $request)
    {
	    $keyword = $request->keyword;    	
    	$get_a = null;
    	$get_b = null;
    	if (!empty($keyword)) {
	        $url_a = 'http://sensor.saifon.my/PrxAndroid/PrxAndroid.svc/GetMobiles/dkshchoc';
	        $get_a = $this->getJson($url_a, $keyword);
	        $url_b = 'http://sensor.saifon.my/PrxAndroid/PrxAndroid.svc/GetMobiles/dkshpharma';
	        $get_b = $this->getJson($url_b, $keyword);
    	}
        return view('search', compact('keyword', 'get_a', 'get_b'));
    }

    public function getUrl()
    {
    	$search = null;
        $url_a = 'http://sensor.saifon.my/PrxAndroid/PrxAndroid.svc/GetMobiles/dkshchoc';
        $get_a = $this->getJson($url_a, $search);
        $url_b = 'http://sensor.saifon.my/PrxAndroid/PrxAndroid.svc/GetMobiles/dkshpharma';
        $get_b = $this->getJson($url_b, $search);
        return json_encode(array_merge($get_a, $get_b));
    }

    public function getJson($url, $keyword)
    {
        $response = file_get_contents($url);
        $response = json_decode($response);
        $props = array();
        foreach ($response as $data) {   
        	if (!empty($keyword)) {
				if( strpos( $data[2]->Value, strtoupper(trim($keyword)) ) !== false ) {
					$arr['coords']['lat'] = $data[3]->Value;
					$arr['coords']['lng'] = $data[4]->Value;
		            $arr['content'] = $data[2]->Value.'<br>Engine '.$data[8]->Value.'<br>'.$data[6]->Value.' KM/H';        
		            $arr['coords']['status_engine'] = $data[5]->Value;
		            $arr['coords']['speed'] = $data[6]->Value;
		            array_push($props, $arr);
				}
        	}
        	else {
	            $arr['coords']['lat'] = $data[3]->Value;
	            $arr['coords']['lng'] = $data[4]->Value;
	            $arr['content'] = $data[2]->Value.'<br>Engine '.$data[8]->Value.'<br>'.$data[6]->Value.' KM/H';        
	            $arr['coords']['status_engine'] = $data[5]->Value;
	            $arr['coords']['speed'] = $data[6]->Value;
	            array_push($props, $arr);        		
        	}
        } 
        return $props;       
    }

    public function getContent()
    {

    }

}
